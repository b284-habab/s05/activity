--1. return the customerName of the customer who are from the philippines

SELECT customerName FROM customers WHERE country="Philippines";

--2. Return the contact lastname of customers with name "La Rochelle Gifts"

SELECT contactLastName FROM customers WHERE customerName="La Rochelle Gifts";

--3. Return the product name and MSRP of the product named "The Titanic"

SELECT productName, MSRP FROM products WHERE productName="The Titanic";

--4. Return the first and last name of the employee whose email is "jfirrelli@classicmodelcars.com"

SELECT firstName, lastName FROM employees WHERE email = "jfirrelli@classicmodelcars.com";

--5. Return the names of customers who have no registered state

SELECT customerName FROM customers WHERE state IS NULL;

--6. Return the first name, last name, email of the employee whose last name is patterson and first name is steve

SELECT firstName, lastName, email FROM employees where lastName = "Patterson" AND firstName = "Steve";

--7. Return customer name, country, and credit limit of customers whose countries are NOT USA AND whose credit limits are greater than 3000

SELECT customerName, country, creditLimit FROM customers WHERE country!='USA' AND creditLimit > 3000;

--8. return the customer numbers of orders whose comments contain the string 'DHL'

SELECT customerNumber FROM orders WHERE comments LIKE '%DHL%';

--9. return the product lines whose text description mentions the phrase 'state of the art'

SELECT productLine FROM productlines WHERE textDescription LIKE '%state of the art%';

--10. return the countries of customers without duplication

SELECT DISTINCT country FROM customers;

--11. return the statuses of orders without duplication

SELECT DISTINCT status FROM orders;

--12. return the customer names and countries of customers whose country is USA, France, or Canada

SELECT customerName, country FROM customers WHERE country IN('USA','France','Canada');

--13. return the first name, lastname, and office's city of employees whose offices are in tokyo

SELECT employees.firstName, employees.lastName, offices.city FROM employees
    JOIN offices ON employees.officeCode = offices.officeCode
    WHERE offices.city="Tokyo";

--14. Return the customer names of customers who were served by the employee named "Leslie Thompson"

SELECT customers.customerName FROM customers
	JOIN employees ON customers.salesRepEmployeeNumber=employees.employeeNumber
    WHERE employees.firstName="Leslie" AND employees.lastName="Thompson";

--15. Return the product names and customer name of products ordered by "Baane Mini Imports"

SELECT products.productName, customers.customerName FROM orderdetails
	JOIN orders ON orderdetails.orderNumber = orders.orderNumber
    JOIN customers ON orders.customerNumber = customers.customerNumber
    JOIN products ON orderdetails.productCode = products.productCode
    
    WHERE customers.customerName="Baane Mini Imports"; 

--16. Return the employee's first names, employees' last names, customer's names, and offices' countries of transactions whose customers and offices are located in the same country

SELECT employees.firstName, employees.lastName, customers.customerName, offices.country FROM offices
	JOIN employees ON offices.officeCode = employees.officeCode
    JOIN customers ON employees.employeeNumber = customers.salesRepEmployeeNumber;


--17. Return the product name and quantity in stock of products that belong to the product line "planes" with stock quantities less than 1000

SELECT products.productName, products.quantityInStock FROM products
	JOIN productlines ON products.productLine = productlines.productLine
    WHERE productlines.productLine="planes" AND products.quantityInStock<1000;


--18. Return the customer's name with a phone number contain "+81"

SELECT customerName, phone FROM customers WHERE customers.phone LIKE "+81%";